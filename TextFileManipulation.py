#!/usr/bin/python

import re
import os
import shutil

logStart = re.compile("^.*[0-9]{8} [0-9]{4}") #start of log message

logfile = open("hosts.real" , "r")
ipAddr = open("ipAddresses.txt" , "w")
newHosts = open("hosts.new", "w+")

getComments = re.compile("#")


#removes all the Comments
withoutComments = ""

for line in logfile:
    if getComments.search(line):
        line = re.sub("#.*" ,"", line)

    data = line.split()
    try:
        line.rstrip("\n\r")
        mac = data.pop()
        print(mac)
    except:
        pass

    line = re.sub('\s+', ' ', line).strip()
    if len(line.strip()) != 0:
        newHosts.write(line + "\n")

    temp = line.split()
    try:
        ipAddr.write(temp.pop(0) + "\n")
    except:
        pass

logfile.close()
newHosts.close()
ipAddr.close()
os.remove("hosts.real")
os.rename("hosts.new","hosts.real")
